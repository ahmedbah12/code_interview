# README #

First Step: Create databse
-------------------------------------------------------------------------------------

Make sure you have mySQL installed on your machine.

And create the databse and user by running the mysql script called 'Script.mysql'.

------------------------------------------------------------------------------------

Second step: Compiling and creating the executable using maven
-----------------------------------------------------------------------------------

Make sure maven is working on your machine.

On the command line go to the directory called clientapp inside repository.

run the following command:

mvn clean compile assembly:single

---------------------------------------------------------------------------------

Third Step: running the executable
----------------------------------------------------------------

Still on the clientapp directory.

run the following command:
java -jar target\Executable-jar-with-dependencies.jar

You will find the result in two files. One csv file containing bad data
with name start with bad_data... and a text file called log file
and contains information.

Note:
-------------------------------------

In case the building using maven does not work I put
the file Executable-jar-with-dependencies.jar into the main directory.

Just double click on it and make sure that the input cvs file is in the same folder.