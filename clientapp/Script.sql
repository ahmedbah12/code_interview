drop user 'javauser'@'localhost';
flush privileges;
CREATE USER 'javauser'@'localhost' IDENTIFIED BY 'javapass'; 
grant usage on *.* to 'javauser'@'localhost' identified by 'javapass'; 
create database Client_DB;
use Client_DB;
grant all privileges on client_DB.* to 'javauser'@'localhost';


CREATE TABLE Info ( 

        id INT(9) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
		A VARCHAR(400),
		B VARCHAR(400),
		C VARCHAR(400),
		D VARCHAR(400),
		E VARCHAR(2000),
		F VARCHAR(400),
		G VARCHAR(400),
		H VARCHAR(400),
		I VARCHAR(400),
		J VARCHAR(400)
		
	);

