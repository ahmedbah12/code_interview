
package com.ms3.interview;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;


public class App {
	
	//dtabase info
	public static final String MYSQL_DRIVER = "com.mysql.jdbc.Driver";
	public static final String MYSQL_URL = "jdbc:mysql://localhost/Client_DB?"
											+ "user=javauser&password=javapass";
	
    public static void main(String[] args) throws IOException {
        
    	
    	
    	//Flags
    	int number=0;
        int problem=0;
        boolean p = false;
        boolean skip=true;
        String time= new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        
        
    	//Writing headers to bad-data
    	FileWriter writer = new FileWriter("bad_data"+"_"+ time +".csv");
        writer.append("A");
        writer.append(",");
        
        writer.append("B");
        writer.append(",");

        writer.append("C");
        writer.append(",");

        writer.append("D");
        writer.append(",");

        writer.append("E");
        writer.append(",");

        writer.append("F");
        writer.append(",");

        writer.append("G");
        writer.append(",");

        writer.append("H");
        writer.append(",");

        writer.append("I");
        writer.append(",");

        writer.append("J");
        writer.append(",");
        
        writer.append('\n');

        //Opening csv file
    	

        String csvFile = "ms3Interview.csv";
        BufferedReader br = null;
        String line = "";
        
        try {
        	

            br = new BufferedReader(new FileReader(csvFile));
            
            while ((line = br.readLine()) != null) {
                 p=false;
            	if(!skip)
            	{
                //Process each  line of the file
                String_split  A = new String_split(line);
                
                String[] result = A.transform_to_array();
                
               
                
                //find bad data
                for(int i=0;i<result.length;i++)
                {
                	if(result[i].isEmpty()) {p = true;break;}
                	
                	
                }
                
                
                //write bad data
                if(p)
                {
                	for(int i=0;i<result.length;i++)
                    {    
                		 writer.append(result[i]);
                	     writer.append(",");
                    }
                    writer.append('\n');
                    problem=problem+1;
                }
                
                //write good data to the database
                if(!p){
                    MySQL_class B = new MySQL_class(MYSQL_DRIVER,MYSQL_URL);
            		B.writeData(result);
            		number=number+1;
            		}
                
            	}  
                
            skip=false;
            }
           

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

       
        writer.close();
        PrintWriter writerfile = new PrintWriter("log_file.txt", "UTF-8");
        writerfile.println("number of records received ");
        writerfile.println(Integer.toString(number+problem));
        //writer.append('\n');
        writerfile.println("number of records with no problems ");
        writerfile.println(Integer.toString(number));
        //writer.append('\n');
        writerfile.println("number of records received with problems ");
        writerfile.println(Integer.toString(problem));
        //writer.append('\n');
        writerfile.close();
        
    }



}