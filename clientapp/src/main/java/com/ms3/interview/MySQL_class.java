package com.ms3.interview;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
//import java.sql.Statement;

public class MySQL_class {
	private final String jdbcDriverStr;
	private final String jdbcURL;
	
	
	
	public MySQL_class(String jdbcDriverStr, String jdbcURL){
		this.jdbcDriverStr = jdbcDriverStr;
		this.jdbcURL = jdbcURL;
		
	}
	
	public void writeData(String[] result) 
	{  try{
		Class.forName(jdbcDriverStr);
		Connection connection = DriverManager.getConnection(jdbcURL);
		//Statement statement= connection.createStatement();
		String query = " insert into client_db.info (A, B,C,D,E,F,G,H,I,J)"
		       + " values (?,?,?,?,?,?,?,?,?,?)";
		 PreparedStatement preparedStmt = connection.prepareStatement(query);
		 preparedStmt.setString(1, result[0]);
	     preparedStmt.setString(2, result[1]);
	     preparedStmt.setString(3, result[2]);
	     preparedStmt.setString(4, result[3]);
	     preparedStmt.setString(5, result[4]);
	     preparedStmt.setString(6, result[5]);
	     preparedStmt.setString(7, result[6]);
	     preparedStmt.setString(8, result[7]);
	     preparedStmt.setString(9, result[8]);
	     preparedStmt.setString(10, result[9]);
	     
	    preparedStmt.execute();
		connection.close(); 
	}
	catch (Exception e) 
	{ e.printStackTrace(); }
		
	}
}
